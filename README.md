# Api Facebook

Este es un sitio para probar la Graph Api de Facebook


## Instrucciones


1.- Descarga el proyecto <br>
2.- Dentro del proyecto corre "composer install" para instalar las dependencias <br>
(si no tienes composer puedes leer como instalarlo y usarlo <a target="_blank" href="https://getcomposer.org/">aqui</a>) <br>
3.- Colocalo en un servidor web y ejectua el proyecto en tu navegador web (EJ: http://localhost/api_facebook/public/) <br>
4.- Usa la siguiente URL con el ID de algun usuario de Facebook "profile/facebook/{id}" <br>
(EJ: http://localhost/api_facebook/public/profile/facebook/1453003752) <br><br>
Te devolvera algo como esto:

{
	"status":200,
	"first_name":"Roberto",
	"last_name":"Sanchez",
	"id":"1453003752"
}


Si es correcto el ID del usuario te devolvera la informacion del mismo, <br>
Si es un usuario incorrecto te devolvera error


## Nota de Programador

Puedes ver toda la logica del Mini Proyecto en templates/get_user.php <br>
para este proyecto use <a target="_blank" href="https://www.slimframework.com/">SLIM PHP</a> para la manipulacion de las rutas<br>
 y la <a target="_blank" href="https://developers.facebook.com/docs/reference/php/">Graph Api de Facebook</a> 