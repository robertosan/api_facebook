<?php

use Slim\Http\Request;
use Slim\Http\Response;
require_once '../vendor/autoload.php';
// Routes
// /profile/facebook/123456

// INDEX
$app->get('/', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});



// GET USER
$app->get('/profile/facebook/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'get_user.php', $args);
});




